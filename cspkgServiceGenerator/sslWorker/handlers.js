 var querystring = require("querystring");
var xml2js = require("xml2js");
var fs = require("fs");
var uuid = require("node-uuid");
var zip = require("./zip.js");
var utils = require('./utils.js');
var util = require('util');
var Q = require("q");

/**
*<?xml version="1.0" encoding="utf-8"?>
*<AzureTest>
*<serviceName>nombreServicio</serviceName>
*<zipCspkg>base64Data</zipCspkg>
*</AzureTest>
*/
function genCspkg( response , postdata ){
    
    util.debug(util.format("INIT {genCspkg}"));

    
    var parser = new xml2js.Parser();
    parser.parseString( postdata , function ( err , result ) {
        if(err){
            response.writeHead(404, {"Content-Type": "text/plain"});
            response.write("404 "+ err);
            response.end();

            util.debug(util.format("END {genCspkg}"));

        }else{
            var serviceName = result['serviceName'];
            var data = new Buffer(result["zipCspkg"], 'base64');
            var identif = uuid.v4();
            fs.writeFileSync(identif+'.zip',data);
            

            var clean = function(){
                fs.unlinkSync( identif + '.zip' );
                fs.unlinkSync( identif + '.cspkg' );
                return utils.remove(identif,true);
            }

            var cspkg = function(){ return callCspack(identif,serviceName);}

            var resp = function() { return genResponse(response , identif , serviceName );}


            //zip.simpleUnzip( identif , identif + '.zip' )
             callUnzip(identif)
            .then(cspkg)
            .then(resp)
            .then(clean)
            .fail(function(err){
                util.error( err );
                response.writeHead(404, {"Content-Type": "text/plain"});
                response.write("404 "+ err);
                response.end();
                util.debug(util.format("END {genCspkg}"));
            });
        }
    });
}


function genResponse ( response , identif , serviceName )
{

    var deferred = Q.defer();

    var base64Data = fs.readFileSync(identif+".cspkg").toString('base64');
    var configuration = fs.readFileSync(identif+"/"+ serviceName +"/ServiceConfiguration.Cloud.cscfg").toString('base64');

    var data = '<?xml version="1.0" encoding="utf-8"?>';
    data += '<AzureTest>';
    data += '<cspkg>'+base64Data+'</cspkg>';
    data += '<configuration>'+configuration+'</configuration>';
    data += '</AzureTest>';


    response.writeHead(200, {"Content-Type": "application/xml"});
    response.write(data);
    response.end();

    util.debug(util.format("END {genCspkg}"));
    
    deferred.resolve();
    return deferred.promise;
}

function callUnzip (identif)
{
    var deferred = Q.defer();

    util.debug(util.format("INIT {CallUnzip:[identif:%s]}",identif));
        
    var exec = require('child_process').exec;

    //llama al fichero cmd
    var child = exec("cscript //nologo descomprimir.vbs "+ identif+".zip",
    function (error, stdout, stderr) {
        if (error != null) 
        {
            util.log('exec error: ' + error);
            fs.writeFileSync("salidaDescomprimir.txt",error);
            util.debug(util.format("END {CallUnzip:[identif:%s]}",identif));
            return deferred.reject(new Error(error));
        }else
        {
            util.debug(util.format("END {CallUnzip:[identif:%s]}",identif));
            deferred.resolve();
        }
    });
        
    return deferred.promise;
}


/**Crea el fichero cmd necesario para llamar al ejecutable cspack.exe
* Node no permite llamar directamente al ejecutable, pues no se encuentra en la ruta base del programa ¿WHY?
*@param name, nombre de la carpeta que contiene el proyecto a empaquetar
*/
function createCmd(name , serviceName )
{
    var deferred = Q.defer();
        
    var content = "@echo off\n";
    content += "cd "+ name +"\\"+serviceName+"\n";
    //content += "..\\..\\sdk\\bin\\cspack.exe /out:..\\"+name+".cspkg ServiceDefinition.csdef\n";
    content += "..\\..\\sdk\\bin\\cspack.exe /out:..\\..\\"+name+".cspkg ServiceDefinition.csdef\n";
    //Escribe el fichero pack.cmd
    fs.writeFile("pack.cmd",content,function(err)
    {
        if(err)
        {
            util.log("Error happens [createCmd]:"+ err);
            fs.writeFileSync("pack.txt",err);
            return deferred.reject(new Error(err));
        }
        util.log("pack.cmd created");
        deferred.resolve();
    });
    return deferred.promise;
}

/**Ejecuta el ejecutable cspack para generar el paquete del proyecto azure
*@param name, nombre del proyecto a empaquetar
*/
function callCspack(name , serviceName ){
    var deferred = Q.defer();

    //genera el fichero cmd
    createCmd(name , serviceName ).then(function(){
        util.log("creating cspack for: "+ name);
        
        var exec = require('child_process').execFile;

        //llama al fichero cmd
        var child = exec('pack.cmd',
            function (error, stdout, stderr) {
        if (error != null) 
        {
            util.log('exec error: ' + error);
            fs.writeFileSync("salida.txt",error);
            fs.writeFileSync("salida2.txt",stderr);
            return deferred.reject(new Error(error));
        }else
        {
            deferred.resolve();
        }
    });
    
    },function(err)
    {
        return deferred.reject(new Error(err));
    });
    return deferred.promise;
}


exports.genCspkg = genCspkg;