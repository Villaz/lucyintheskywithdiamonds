
function route( handle , pathname , response , postdata )
 {
    console.log("Vamos a encaminar la peticion para " + pathname);
     if (typeof handle[pathname] === 'function') {
         return handle[pathname]( response , postdata );
     } else {
         response.writeHead(404, {"Content-Type": "text/plain"});
         response.write("404 Not Found");
         response.end();
     }
 }

exports.route = route;