var server = require("./serverSSL.js");
var router = require("./route.js");
var handler = require("./handlers.js");

var port = process.env.port || 1337;

var handlers = {
     "/cspkg" : handler.genCspkg
}

server.start(port,router.route, handlers);


