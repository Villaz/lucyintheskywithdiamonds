var https = require('https');
var fs = require('fs');
var url = require('url');

var options = {
  key: fs.readFileSync('./keys/key.pem'),
  cert: fs.readFileSync('./keys/cert.pem')
};

function start(port , route , handle){

	

	function onCall(request, response) {

		var pathname = url.parse(request.url).pathname;
		var postdata = "";
       
        request.setEncoding('utf8');
        request.addListener('data', function (chunk) {
            postdata += chunk;
		});
        
        request.addListener('end', function () {
            route( handle , pathname , response , postdata );
		});

    }

    var server = https.createServer(options, onCall );

	server.listen(port,function(){console.log("Server listening on port "+ port)});

}

exports.start = start;