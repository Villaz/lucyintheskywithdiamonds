var https = require('https');
var fs = require('fs');
var querystring = require('querystring');
var azure = require('azure');
var Q = require('q');
var util = require('util');
xml2js = require('xml2js');

var exports = module.exports = AzureRest;

AzureRest.prototype.cert 			= "";
AzureRest.prototype.key				= "";
AzureRest.prototype.userId 			= "";

var azu = "";

var AZURE_MANAGEMENT_CORE = 'management.core.windows.net';

/**
* Constructor de AzureRest
* @param [string]	{userId}		Identificador del usuario en azure.
* @param [string]	{azureRestCert}	Dirección local del certificado del usuario.
* @param [string]	{azureRestKey}	Dirección local de la clave del usuario.
*/
function AzureRest ( userId , azureRestCert , azureRestKey )
{
	this.userId = userId;
	this.cert = fs.readFileSync( azureRestCert );
	this.key = fs.readFileSync( azureRestKey );
	azu = this;
}




/**
* Crea un nuevo container para almacenar datos de tipo Blob.
* Si storageAccessKey no es introducido, entonces se realiza una llamada REST para obtener la clave.
*
* @this {AzureRest}
*
* @param {string}	[name]				Nombre del container
* @param {string}	[storageAccount]	Nombre del storage donde se va a crear el almacen
* @param {string}	[storageAccessKey]	Clave utilizada para conectarse al storage
*/
AzureRest.prototype.createBlobContainer = function( name , storageAccount , storageAccessKey )
{
	//clousure utilizado para llamar en los diferentes puntos de la funcion
	//llama al servicio de azure para crear el blobcontainer en caso de no existir
	var createBlob = function(){
		util.debug( util.format("INIT {createBlobContainer:[name:%s]}" , name ) );

		var blobService = azure.createBlobService( storageAccount , storageAccessKey );
		blobService.createContainerIfNotExists(name, {publicAccessLevel : 'blob'} , function(error){
			if(error){
				deffered.reject( new Error( error["code"] ) );
			}
			deferred.resolve(name);
		});
	}


	var deferred = Q.defer();
	if( storageAccessKey == null || storageAccessKey == undefined ){
		this.getStorageAccountKeys( storageAccount ).then(
				function( keys ){
					storageAccessKey = keys['Primary'];
					createBlob();
					
				},
					function( err ){
					deferred.reject( new Error( err ) );
				});
	}else{
		createBlob();
	}
	return deferred.promise;
}

/**
* Retorna una lista de todos los storages.
* @this   {AzureRest}
* @return {promise}
*/
AzureRest.prototype.listStorages = function()
{

	util.debug ( "[listStorage]" );

	var deferred = Q.defer();

	var options = {
		host: AZURE_MANAGEMENT_CORE ,
		path: util.format( '/%s/services/storageservices' , this.userId ),
		port: 443,
		method: 'GET',
		cert: this.cert ,
		key: this.key ,
		agent: false
	};

	var req = https.request( options , function( res ){
		res.on('data',function( d ){
			var parser = new xml2js.Parser();
				parser.parseString( d , function ( err , result ) {
					if( err ){
						deferred.reject( new Error( err ) );
					}else{
						deferred.resolve( result );
					}
           		});
		});
	});

	req.setHeader( "x-ms-version","2011-10-01" );
	req.end();

	req.on( 'error' , function( e ){
		deferred.reject( new Error( e ) );
	});

	return deferred.promise;
}


/**
* Crea una nueva cuenta de almacenamiento.
* @this   {AzureRest}
* @param  {string}		[name] Nombre de la cuenta de almacenamiento
* @return {promise}
*/
AzureRest.prototype.createStorage = function( name )
{
	util.debug( util.format( "INIT {createStorage:[name:%s]}" , name ) );

	var deferred = Q.defer();

	var name64 = new Buffer( name ).toString( 'base64' );

	var data =	'<?xml version="1.0" encoding="utf-8"?>'+
				'<CreateStorageServiceInput xmlns="http://schemas.microsoft.com/windowsazure">'+
   				'<ServiceName>'+ name +'</ServiceName>'+
   				'<Label>'+ name64 +'</Label>'+
   				//'<AffinityGroup>affinity-group-name</AffinityGroup>'+
  				'<Location>West Europe</Location>'+
				'</CreateStorageServiceInput>';

	var options = {
		host: AZURE_MANAGEMENT_CORE,
		path: util.format( '/%s/services/storageservices' , this.userId ),
		port: 443,
		method: 'POST',
		cert: this.cert ,
		key: this.key ,
		agent: false
	};

	var req = https.request( options , function( res ){
		
		res.on( 'data' , function( d ){
			console.log("Entro aqui");
			var parser = new xml2js.Parser();
			parser.parseString( d , function ( err , result ) {
				if( err ){
					util.debug( util.format( "END {createStorage:[name:%s]}" , name ) );
					deferred.reject( new Error( err ));
				}else{
					
					util.debug( util.format( "END {createStorage:[name:%s]}" , name ) );
					deferred.reject( new Error( result["Message"] ));
										
				}
			});
		});


		res.on ('end' , function (){
			var requestId = res.headers[ "x-ms-request-id"];
			checkEndFunction(azu,requestId).then(function(c){
				deferred.resolve();
			});
		});
	});

	req.setHeader( "x-ms-version" , "2011-10-01" );
	req.setHeader( "Content-Type" , "application/xml" );
	req.write( data );
	req.end();

	req.on( 'error' , function( e ){
		console.error( e );
		deferred.reject( new Error( e ) );
	});
	console.log("Salgo de aqui");
	return deferred.promise;
}

/**
* Retorna las clave primaria y sencundaria del storage indicado, con la forma {Primary:'key', Secondary:'key'}
*
* @this {AzureRest}
* @param {string}	[storage]	nombre del storage del cual se quieren conocer las claves
*/
AzureRest.prototype.getStorageAccountKeys = function ( storage )
{
	util.debug(util.format("[storage:%s,userId:%s]" , storage,this.userId));
	
	var deferred = Q.defer();

	var options = {
		host: AZURE_MANAGEMENT_CORE,
		path: util.format('/%s/services/storageservices/%s/keys', this.userId , storage),
		port: 443,
		method: 'GET',
		cert: this.cert,
		key: this.key,
		agent: false
	};

	var req = https.request(options,function(res){
		res.on( 'data' , function( d ) {
			var parser = new xml2js.Parser();
			parser.parseString( d , function ( err , result ) {
				if( err ){
					deferred.reject( new Error( err ) );
				}else{
					if( result['StorageServiceKeys'] == undefined )
						deferred.reject( new Error( result['Message'] ));
					else
						deferred.resolve( result['StorageServiceKeys'] );
				}
           	});
		});
	});

	req.setHeader( "x-ms-version","2011-10-01" );
	req.end();

	req.on( 'error',function( e ) {
		console.error( e );
		deferred.reject( new Error( e ));
	});

	return deferred.promise;
}

/**
* Retorna una lista de todos los contenedores en un storage.
* @this   {AzureRest}
* @param  {string}		[storage]			Storage del cual retornar los containers.
* @param  {string}		[storageAccesKey]	Clave de acceso al storage. Opcional. En caso de no introducirla se realiza una petición a Azure.
* @return {promise}	
*/
AzureRest.prototype.listContainer = function (storage , storageAccessKey)
{
	util.debug( util.format( "[getListContainer,{storage:%s}]" , storage ));

	var listContainer = function(){
		var blobService = azure.createBlobService( storage , storageAccessKey );

		blobService.listContainers( storage,function(err , containers , nextMarker , response ){
			if( err ){
				util.error( err );
				deferred.reject(new Error( err ));
			}else{
				util.debug( util.format( "[containers:%s]" , containers ));
				util.debug( util.format( "[nextMarker:%s]" , nextMarker ));
				util.debug( util.format( "[response:%s" , response ));
				deferred.resolve( containers );
			}
		});
	}


	var deferred = Q.defer();


	if( storageAccessKey == null || storageAccessKey == undefined ){
		this.getStorageAccountKeys( storage ).then(
			function( keys ){
				storageAccessKey = keys['Primary'];
				listContainer();
			}, function( err ){
				deferred.reject( new Error( err ));
			});
	}else{
		listContainer();
	}
	

	return deferred.promise;
}

/**
* Retorna una lista de todos los blobs contenidos en un container.
* @this   {AzureRest}
* @param  {string}		[storage]		Storage donde se encuentran los blobs.
* @param  {string}		[container]		Container donde se encuentran los blobs.
* @param  {string}		[storageAccesKey]	Clave de acceso al storage. Opcional. En caso de no introducirla se realiza una petición a Azure.
* @return {promise}
*/
AzureRest.prototype.listBlobs = function ( storage , container, storageAccessKey ){
	
	util.debug(util.format( "[getListBlobs:{storage:%s,container:%s}]" , storage , container ));

	var deferred = Q.defer();

	var listBlobs = function(){
		
		var blobService = azure.createBlobService( storage , storageAccessKey );
		blobService.listBlobs( container , function( err , blobs , response ){
			if( err ){
				util.error( err );
				deferred.reject( new Error( err ));
			}else{
				util.debug( util.format( "blobs:%s" , blobs ));
				deferred.resolve( blobs );
			}
		});
	}


	if( storageAccessKey == null || storageAccessKey == undefined ){
		this.getStorageAccountKeys( storage ).then(
			function( keys ){
				storageAccessKey = keys['Primary'];
				listBlobs();
			}, function( err ){
				deferred.reject( new Error( err ));
			});
	}else{
		listBlobs();
	}

	return deferred.promise;
}

/**
* Sube un fichero a un Blob de azure
* @this   {AzureRest}
* @param  {string}		[storage]			Cuenta de almacenamiento donde se va a subir el fichero
* @param  {string}		[container]			Contenedor donde se va a subir el fichero. Parecido a una carpeta.
* @param  {string}		[name]				Nombre del blob a crear.
* @param  {string}		[file]				Dirección local del fichero a subir
* @param  {string}		[storageAccessKey]	Clave de acceso al almacenamiento. Opcional . Si no se indica se realiza una petición a Azure para recuperarla.
* @return {promise}
*/
AzureRest.prototype.uploadFile = function ( storage , container , name , file , storageAccessKey ){

	util.debug( util.format( "INIT {uploadFile :[storage:%s , file:%s]}", storage , file ));

	var deferred = Q.defer();

	var upload = function()
	{
		var blobService = azure.createBlobService( storage , storageAccessKey);
		blobService.createBlockBlobFromFile(container , name , file , function( err , serverBlob ){
			if( err ){
				console.log(err);
				util.debug( util.format( "END {uploadFile :[storage:%s , file:%s , err:%s]}", storage , file ,err["code"]));
				deferred.reject( new Error( err["code"]));
			}else{
				util.debug( util.format( "END {uploadFile :[storage:%s , file:%s]}", storage , file ));
				var url = util.format("http://%s.blob.core.windows.net/%s/%s",storage,container,name);
				deferred.resolve( url );
			}
		});
	}

	if( storageAccessKey == null || storageAccessKey == undefined ){
		this.getStorageAccountKeys( storage ).then(
			function( keys ){
				storageAccessKey = keys['Primary'];
				upload();
			}, function( err ){
				util.debug( util.format( "END {uploadFile :[storage:%s , file:%s , err:%s]}", storage , file ,err));
				deferred.reject( new Error( err ));
			});
	}else{
		upload();
	}

	return deferred.promise;

}

/**
* Descarga un fichero desde un Blob de azure
* @this   {AzureRest}
* @param  {string}		[storage]			Cuenta de almacenamiento donde se encuentra el fichero
* @param  {string}		[container]			Contenedor donde encuentra el fichero. Parecido a una carpeta.
* @param  {string}		[name]				Nombre del blob a descargar.
* @param  {string}		[file]				Dirección local donde guardar el blob.
* @param  {string}		[storageAccessKey]	Clave de acceso al almacenamiento. Opcional . Si no se indica se realiza una petición a Azure para recuperarla.
* @return {promise}
*/
AzureRest.prototype.downloadFile = function (storage , container , name , file , storageAccessKey){
	util.debug( util.format( "[downloadFile {storage:%s , file:%s}]", storage , file ));

	var deferred = Q.defer();

	var download = function()
	{
		var blobService = azure.createBlobService( storage , storageAccessKey);
		blobService.getBlobToFile(container , name , file , function( error, blockBlob, response ){
			if( error ){
				util.error( error );
				deferred.reject( new Error( error ));
			}else{
				util.debug( blockBlob );
				deferred.resolve( blockBlob );
			}
		});
	}

	if( storageAccessKey == null || storageAccessKey == undefined ){
		this.getStorageAccountKeys( storage ).then(
			function( keys ){
				storageAccessKey = keys['Primary'];
				download();
			}, function( err ){
				deferred.reject( new Error( err ));
			});
	}else{
		download();
	}

	return deferred.promise;
}

/**
* Elimina un Blob de azure
* @this   {AzureRest}
* @param  {string}		[storage]			Cuenta de almacenamiento donde se encuentra el fichero
* @param  {string}		[container]			Contenedor donde encuentra el fichero. Parecido a una carpeta.
* @param  {string}		[name]				Nombre del blob.
* @param  {string}		[storageAccessKey]	Clave de acceso al almacenamiento. Opcional . Si no se indica se realiza una petición a Azure para recuperarla.
* @return {promise}
*/
AzureRest.prototype.deleteBlob = function (storage, container , name , storageAccessKey){
	util.debug( util.format( "[deleteBlog {storage:%s , container:%s , name:%s}]", storage , container , name ));

	var deferred = Q.defer();

	var deleteClousure = function()
	{
		var blobService = azure.createBlobService( storage , storageAccessKey );
		blobService.deleteBlob( container , name ,function( error, isSuccessful , response ){
			if( error ){
				util.error( error );
				deferred.reject( new Error( error ));
			}else{
				util.debug( isSuccessful );
				deferred.resolve( isSuccessful );
			}
		});
	}

	if( storageAccessKey == null || storageAccessKey == undefined ){
		this.getStorageAccountKeys( storage ).then(
			function( keys ){
				storageAccessKey = keys['Primary'];
				deleteClousure();
			}, function( err ){
				deferred.reject( new Error( err ));
			});
	}else{
		deleteClousure();
	}

	return deferred.promise;
}



/**
* Crea un nuevo Servicio
* @this   {AzureRest}
* @param  {string}		[name] Nombre del servicio
* @return {promise}
*/
AzureRest.prototype.createService = function ( name )
{
	util.debug( util.format( "INIT {createService:[name:%s]}" , name ) );

	var deferred = Q.defer();

	var name64 = new Buffer(name).toString('base64');

	//Creación del XML, futuras versiones permitirán definir una descripción y un grupo de afinidad.
	var data = 	'<?xml version="1.0" encoding="utf-8"?>'+
				'<CreateHostedService xmlns="http://schemas.microsoft.com/windowsazure">'+
				'<ServiceName>'+name+'</ServiceName>'+
  				'<Label>'+name64+'</Label>'+
  				//'<Description>description</Description>'+
  				'<Location>West Europe</Location>'+
 				//'<AffinityGroup>affinity-group</AffinityGroup>'+
				'</CreateHostedService>';

	var options = {
		host: AZURE_MANAGEMENT_CORE,
		path: util.format( '/%s/services/hostedservices' , this.userId ),
		port: 443,
		method: 'POST',
		cert: this.cert,
		key: this.key,
		agent: false
	}

	var req = https.request(options,function(res){
		res.on('data',function(d){
			var parser = new xml2js.Parser();
			parser.parseString( d , function ( err , result ) {
				if( err ){
					util.debug( util.format( "END {createService:[name:%s]}" , name ) );
					deferred.reject( new Error( err ) );
				}else{
					if( result['Message'] != undefined ){
						util.debug( util.format( "END {createService:[name:%s]}" , name ) );
						deferred.reject( new Error( result['Message'] ));
					}
				}
           	});
		});

		res.on('end',function(){
			util.debug( util.format( "END GOOD {createService:[name:%s]}" , name ) );
			deferred.resolve();
		});
	});

	req.setHeader("x-ms-version","2011-10-01");
	req.setHeader("Content-Type","application/xml");
	req.write(data);
	req.end();

	req.on('error',function(e){
		util.debug( util.format( "END {createService:[name:%s]}" , name ) );
		util.error("Ha fallado en el request");
		console.log(e);
		deferred.reject( new Error( e ) );
	});

	return deferred.promise;
}

/**
* Crea un nuevo deployment en Azure a partir de un fichero cspkg almacenado en el blob.
* @this {AzureRest}
* @param {string}	[name]					Nombre del deployment
* @param {string}	[packageUrl] 			URL del cspkg
* @param {string}	[serviceName]			Nombre del servicio en el cual introducir el deployment
* @param {string}	[configurationFile]		Dirección donde se encuentra el fichero de configuración del despliegue
* @param {string}	[deploymentSlot]		Indica la forma de despliegue , staging o production / opcional, por defecto production
* @param {string}	[startDeployment]		Indica si el despliegue se inicializa inmediamente. Opcional, por defecto false.
* @param {string}	[treatWarningsAsError]	Indica si el despliegue se ha de finalizar al ocurrir warnings . Opcional por defecto false.
* @return {promise}
*/
AzureRest.prototype.createDeployment = function(name , packageUrl , serviceName , configurationFile , deploymentSlot , startDeployment , treatWarningsAsError ){

	util.debug(util.format("[createDeployment:{name:%s,packageUrl:%s,serviceName:%s,deploymentSlot:%s}]",name,packageUrl,serviceName,deploymentSlot));

	var deferred = Q.defer();

	if( deploymentSlot == null || deploymentSlot == undefined)
		deploymentSlot = "production";

	if(startDeployment == null || startDeployment == undefined)
		startDeployment = false;
	
	if(treatWarningsAsError == null || treatWarningsAsError == undefined)
		treatWarningsAsError = false;

	var name64 = new Buffer(name).toString('base64');
	var configuration64 = new Buffer(fs.readFileSync(configurationFile)).toString('base64');

	var data = 	'<?xml version="1.0" encoding="utf-8"?>'+
				'<CreateDeployment xmlns="http://schemas.microsoft.com/windowsazure">'+
				'<Name>'+name+'</Name>'+
				'<PackageUrl>'+packageUrl+'</PackageUrl>'+
				'<Label>'+name64+'</Label>'+
				'<Configuration>'+configuration64+'</Configuration>'+
				'<StartDeployment>'+startDeployment+'</StartDeployment>'+
				'<TreatWarningsAsError>'+treatWarningsAsError+'</TreatWarningsAsError>'+
				'</CreateDeployment>';


	var options = {
		host: 'management.core.windows.net',
		path: util.format('/%s/services/hostedservices/%s/deploymentslots/%s',this.userId,serviceName,deploymentSlot),
		port: 443,
		method: 'POST',
		cert: this.cert,
		key: this.key,
		agent: false
	}

	//se trata de una petición asíncrona, siempre ha de devolver un 200OK
	//para conocer el progreso de la operación se ha de llamar a getStatus
	var req = https.request(options,function(res){
		
		//si la respuesta contiene datos se ha producido un error en el lanzamiento del despliegue
		res.on('data',function(d){
			var parser = new xml2js.Parser();
			parser.parseString( d , function ( err , result ) {
				if( err ){
					deferred.reject( new Error( err ) );
				}else{
					if( result['Message'] != undefined )
						deferred.reject( new Error( result['Message'] ));
					else
						deferred.reject( result );
				}
           	});
		});

		//al finalizar la petición recogemos el x-ms-request-id
		res.on('end',function(d){
			var requestId = res.headers[ "x-ms-request-id"];
			checkEndFunction(azu,requestId).then(function(c){
				util.debug(util.format("END [createDeployment:{name:%s}]",name));
				deferred.resolved(c);
			});
		});
	});

	req.setHeader("x-ms-version","2011-10-01");
	req.setHeader("Content-Type","application/xml");
	req.write(data);
	req.end();

	req.on( 'error' , function( e ){
		util.error(e);
		deferred.reject( new Error( e ));
	});
	return deferred.promise;
}

/**
* Añade un nuevo certificado a un servicio. Es necesario para realizar el despliegue de los roles.
* El certificado a utilizar ha de encontrarte en el contenedor de certificados de azure. (Consola de administración).
* Se trata de un servicio asincrono por lo cual hay que esperar a que finalice para continuar
* @this   {AzureRest}
* @param  {string}		[file]			Dirección local donde se encuentra el certificado. Ha de ser pfx.
* @param  {string}		[password]		Contraseña con la cual se encuentra protegida la clave.
* @param  {string}		[nameService]	Nombre del servicio al cual se va a vincular el certificado.
* @return {promise}
*/
AzureRest.prototype.addCertificate = function( file , password , nameService ){

	util.debug(util.format("INIT {addCertificate:[file:%s]}",file));

	var deferred = Q.defer();

	var certificate64 = new Buffer(fs.readFileSync(file)).toString('base64');

	var data = 	'<?xml version="1.0" encoding="utf-8"?>'+
				'<CertificateFile xmlns="http://schemas.microsoft.com/windowsazure">'+
  				'<Data>'+certificate64+'</Data>'+
  				'<CertificateFormat>pfx</CertificateFormat>'+
  				'<Password>'+password+'</Password>'+
				'</CertificateFile>';

	//console.log(data);
	var options = {
		host: 'management.core.windows.net',
		path: util.format( '/%s/services/hostedservices/%s/certificates' , this.userId , nameService ),
		port: 443,
		method: 'POST',
		cert: this.cert,
		key: this.key,
		agent: false
	}

	var req = https.request( options , function( res ){
		
		res.on( 'data' , function( d ){
			var parser = new xml2js.Parser();
			parser.parseString( d , function ( err , result ) {
				if( err ){
					deferred.reject( new Error( err ));
				}else{
					if(result["Code"] == "InvalidXmlRequest"){
						util.debug(util.format("END {addCertificate:[file:%s, error:%s]}",file,result["Code"]));
						deferred.reject( new Error ( result["Message"] ));
					}else{
						util.debug(util.format("END GOOD {addCertificate:[file:%s]}",file));
						deferred.resolve( d );
					}
				}
			});

			
		});

		res.on ('end', function(){
			var requestId = res.headers[ "x-ms-request-id"];
			checkEndFunction(azu,requestId).then(function(c){
				util.debug(util.format("END GOOD {addCertificate:[file:%s]}",file));
				deferred.resolve();
			});
		});
	});

	req.setHeader( "x-ms-version" , "2009-10-01" );
	req.setHeader( "Content-Type" , "application/xml" );
	req.write( data );
	req.end();

	req.on( 'error' , function( e ){
		var parser = new xml2js.Parser();
		parser.parseString( e , function ( err , result ) 
		{
			if( err )
			{
				util.debug(util.format("END {addCertificate:[file:%s]}",file));
				deferred.reject( new Error( e ));
			}else{
				util.debug(util.format("END {addCertificate:[file:%s]}",file));
				deferred.reject( new Error( result["Message"] ));
			}
		});
	});

	return deferred.promise;
}


/**
* Retorna una lista con los servicios que se encuentran en la suscripción del usuario.
* @this {AzureRest}
* @return {promise}
*/
AzureRest.prototype.listServices = function()
{
	util.debug("[listServices]");

	var deferred = Q.defer();

	var options = {
		host: 'AZURE_MANAGEMENT_CORE',
		path: util.format( '/%s/services/hostedservices' , this.userId ),
		port: 443,
		method: 'GET',
		cert: this.cert,
		key: this.key,
		agent: false
	}

	var req = https.request( options , function( res ){
		res.on( 'data' , function( d ){
			process.stdout.write(d);
			deferred.resolve( d );
		});
	});

	req.setHeader("x-ms-version","2011-10-01");
	req.end();

	req.on( 'error' , function( e ){
		util.error( e );
		deferred.reject( new Error( e ));
	});

	return deferred.promise;
}


AzureRest.prototype.getStatus = function(requestId){

	util.debug(util.format("INIT {getStatus:[requestId:%s]}",requestId));

	var deferred = Q.defer();
	
	var options = {
		host: AZURE_MANAGEMENT_CORE,
		path: util.format( '/%s/operations/%s' , this.userId , requestId ),
		port: 443,
		method: 'GET',
		cert: this.cert,
		key: this.key,
		agent: false
	}

	var req = https.request( options , function( res ){
		res.on( 'data' , function( d ){
			var parser = new xml2js.Parser();
			parser.parseString( d , function ( err , result ) 
			{
				if( err )
				{
					util.debug(util.format("END {getStatus:[requestId:%s]}",requestId));
					deferred.reject( new Error( e ));

				}else
				{
					util.debug(util.format("END {getStatus:[requestId:%s]}",requestId));
					
					var data = {"status":"","message":""};
					data.status = result["Status"];
					if(result["Error"] != undefined)
						data.message = result["Error"]["Message"];
					deferred.resolve( data );
				}
			});
		});

	});

	req.setHeader("x-ms-version","2011-10-01");
	req.end();

	req.on( 'error' , function( e ){
		util.debug(util.format("END {getStatus:[requestId:%s]}",requestId));
		deferred.reject( new Error( e ));
	});

	return deferred.promise;
}

function checkEndFunction(azure,requestId,deferred){

	if(deferred == undefined)
		deferred = Q.defer();

		
		var resultado = "";

		var sleep = function (millisegundos) {
                var inicio = new Date().getTime();
                while ((new Date().getTime() - inicio) < millisegundos){}
        }

    	azure.getStatus(requestId).then(function(status)
    	{
    		console.log(status);
    		if(status["status"] == "InProgress")
    		{
    			sleep(10000);
    			
    			checkEndFunction(azure,requestId,deferred);
    		}else{
    			if(status["status"] == "Succeeded")
    				deferred.resolve(status["status"]);
    			else
    				deferred.reject(status["message"]);
    		}
    	});
    
    return deferred.promise;
} 


//var azu = new AzureRest('569ead87-1687-41e0-b571-1466c0a27de3','/Users/luis/certs/cert.pem','/Users/luis/certs/key.pem');
//azu.createService("servicepruebaluisvillazon").then(function(v){console.log(v)}).fail(function(e){console.log(e)});

//Test
/*var azu = new AzureRest('3b036372-5071-4db3-a6d2-53ad3edef147','cert.pem','key.pem');


var createStorage = function () { return azu.createStorage("serviceluisvillazon"); }
var createCertificate = function() { return azu.addCertificate("mycert.pfx","Espronceda","servicepruebaluisvillazon");}
var createBlobContainer = function() {return azu.createBlobContainer('probando','serviceluisvillazon');}
var uploadFile = function(){ return azu.uploadFile('serviceluisvillazon','probando','package','d9b76e2a-892b-4b79-85b3-3cde7c289199.cspkg');}
var createDeployment = function(){ return azu.createDeployment("deploymentluisVillazon","http://serviceluisvillazon.blob.core.windows.net/probando/package","servicepruebaluisvillazon","ServiceConfiguration.cscfg");}
*/

//checkEndFunction(azu,"7fc3e6842e6d4aa8a80ddbfe112ec113").then(function(c){console.log("wiii")});


/*
azu.createService("servicepruebaluisvillazon")
	.then(createCertificate)
	.then(createStorage)
	.then(createBlobContainer)
	.then(uploadFile)
	.then(createDeployment)
	.then(function(v){console.log("conseguido "+v);})
	.fail(function(err){console.log("Jodido:"+err)});

*/
//creacion de un nuevo servicio
//azu.createService("servicepruebaluisvillazon").then(function(v){console.log(v)}).fail(function(e){console.log(e)});
//azu.createStorage("serviceluisvillazon").then(function(v){console.log(v)}).fail(function(e){console.log(e)});

//añade certificado
//azu.addCertificate("mycert.pfx","*****","servicepruebaluisvillazon").then(function(v){console.log(v)}).fail(function(e){console.log(e)});

//crea un storage
//azu.createStorage("serviceluisvillazon").then(function(v){console.log(v)}).fail(function(e){console.log(e)});

//crea un blobContainer
//azu.createBlobContainer('probando','serviceluisvillazon').then(function(v){console.log(v)}).fail(function(err){console.log(err)});


//azu.getListContainer('blobluisvillazon').then(function(v){console.log(v)});

/*
azu.uploadFile('serviceluisvillazon','probando','package','cloud_package.cspkg').then(
	function(v)	{
		console.log("va bien la cosa");
		console.log(v)
	}).fail(function(err){
		console.log("jodida");
		console.log(err)});
*/

//El nombre de los ficheros será http://serviceluisvillazon.blob.core.windows.net/probando/package

/*
azu.listBlobs('serviceluisvillazon','probando').then(function(v){
	for(a in v){
		console.log(a);
		console.log(v[a]);
	}
});
*/

//azu.deleteBlob('serviceluisvillazon','probando','package').then(function(v){console.log('finalizado')}).fail(function(err){console.log(err)});
//azu.downloadFile('serviceluisvillazon','probando','package','./bajado.cspkg').then(function(v){console.log('finalizado')}).fail(function(err){console.log(err)});

//azu.addCertificate('mycert.pfx','*****','storageluisvillazon').then(function(v){}).fail(function(err){console.log(err)});


//azu.createDeployment("deploymentluisVillazon","http://serviceluisvillazon.blob.core.windows.net/probando/package","servicepruebaluisvillazon","ServiceConfiguration.Cloud.cscfg").
//then(function(v){console.log(v)}).fail(function(err){console.log(err)});
//azu.getStatus("95ae8476b3e143948a17549c193c9cfc").then(function(v){console.log(v);},function(err){console.log(e);});
//util.debug(checkEndFunction(azu,"95ae8476b3e143948a17549c193c9cfc"));
