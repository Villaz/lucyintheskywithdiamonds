var azure = require("azure");
var net   = require("net");
var http  = require("http");
//var q     = require("q");



/** COSAS A CONSIDERAR **/
	// kkk el nombre del rol no deber�a ser hardcoded. A pensar...
	// kkk por defecto consideraremos un rol interno, el cual llamaremos desde rolsupport con una llamada particular (transparencia)



exports = module.exports;

// Para hacer pruebas
exports.hola = function(){
	console.log("asd");
}
console.log("fgh");
 

// Comienzo de lo que se cree que es la respuesta 'heartbeat' para el FC de azure
var port = process.env.port || 1337;

http.createServer(function (req, res) {
	es.writeHead(200, { 'Content-Type': 'text/plain' });
  	res.end('Hello World\n');
}).listen(port);
// Fin del hacedor de latidos de coraz�n (coraz�n?)


// Nos devuelve la informacion de esta instancia en particular del rol
exports.currentRole = function(callback){
	azure.RoleEnvironment.getCurrentRoleInstance(function(error,instance){
		if(error){
			callback(error);
		}else{
			callback(undefined,instance);
		}
	});
}

// Lista de todos los roles
exports.roles = function (callback) {
  azure.RoleEnvironment.getRoles(function (error, roles) {
    if (error) {
      callback(error);
    } else {
		callback(undefined,roles);
    }
  });
};

// Devuelve todas las instancias de un rol, 
// a partir del nombre del rol y del listado de todas las instancias de todos los roles
exports.instancesFromRole = function(name,callback){
	exports.roles(function(err,roles){
		if(err)
		{
			callback(err);
		}else
		{
			var found = false;
			for(var rol in roles)
			{
				if(rol == name)
				{
					found = true;
					callback(undefined,roles[rol].instances);
				}
			}
			if(!found)
			{
				callback("unknown role");
			}
		}
	});
}

//le das el nombre del rol, el nombre del endpoint y te devuelve todas 
//las direcciones y puertos de ese endpoint en todas las instancias
exports.EndpointFromRole = function (nameRole,endpointName,callback)
{
	exports.instancesFromRole(nameRole,function(err,instances)
	{
		if(err)
		{
			callback(err);
		}
		else{
			var instanceEndpoint = {}
			for(var instance in instances)
			{
				if(instances[instance].endpoints[endpointName])
				{
					instanceEndpoint[instance] = {"port":instances[instance].endpoints[endpointName].port, "address":instances[instance].endpoints[endpointName].address};
				}
			}
			callback(undefined,instanceEndpoint);
		}
	});
}


// Obtiene la direcci�n en la que est� escuchando la instancia de un rol
exports.getListenAddress = function(endpointName, callback) {
	RoleEnvironment.getCurrentRoleInstance(function( inErr, instance) {
		if (inErr || !instance['endpoints'] || ! instance['endpoints'][endpointName]) {
			callback("Error, could not get current role instance endpoint '"+ endpointName + "', error: " + inErr + "\r\n", null, null);
		} else {
            var currentEndpoint = instance['endpoints'][endpointName];
            callback(null, currentEndpoint['address'], currentEndpoint['port']);
        }
    }); 
}


/*
/**
* Retrieves the settings in the service configuration file.
* A role's configuration settings are defined in the service definition file. Values for configuration settings are
* set in the service configuration file.
* 
* @param {function(error, configurationSettings)} callback The callback function.

RoleEnvironment.getConfigurationSettings = function (callback) {
  validateNodeVersion();
  validateCallback(callback);

  RoleEnvironment._initialize(function (error) {
    if (error) {
      callback(error);
    } else {
      callback(undefined, currentEnvironmentData.configurationSettings);
    }
  });
};





*/


