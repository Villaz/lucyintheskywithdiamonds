var fs 	= require("fs");
var Q 	= require("q");
var utils = require("./utils.js");
var util = require("util");
var zip = require("./zip.js");

// falta un punto en el que tenemos que leer los credenciales y asignarlos al service configuration
// referencia http://msdn.microsoft.com/es-es/library/ee758710.aspx
// La configuración de un rol consta de pares nombre-valor que se 
// declaran en el archivo de definición del servicio y se establecen en el archivo de configuración del servicio.

function createStruct ( deployDir ) {

	util.debug(util.format("INIT: {createStruct:[deployDir:%s]}",deployDir));

	var deferred	=  Q.defer();

	try{

		var deployJSON	= JSON.parse( fs.readFileSync ( "./" + deployDir ));
		var serviceJSON = JSON.parse( fs.readFileSync ( "./tmp/service.json" ));
		var serviceDir = serviceJSON["name"];

		fs.mkdirSync(serviceDir);

		var serviceConfiguration = buildServiceConfiguration ( serviceDir , deployJSON , serviceJSON);
		var serviceDefinition 	 = buildServiceDefinition ( serviceDir , deployJSON , serviceJSON );
		
		
		var numRoles = function ( ){
			var count = 0;
			for(var rol in serviceJSON.roles )
				count++;
			return count;
		}

		var counter = 0;
		for ( var rol in serviceJSON.roles ){
			
			var pathRol = serviceDir + '/' + rol;//serviceJSON.roles[rol].name;
			fs.mkdirSync( pathRol );
			var files = fs.readdirSync( './tmp/' + serviceJSON.roles[rol].name);
			for( file in files ){
				var ori = "./tmp/" + serviceJSON.roles[rol].name + "/" + files[file];
				var dest = pathRol+'/'+files[file];
				
				if (files[file].substring(files[file].length - 4 ) != "json")
					utils.copyFile( ori , dest );
			
			}

			//añade el ejecutable de node a cada rol
			utils.copyFile("./bin/node.exe",pathRol+"/node.exe");
			utils.copyFile("./bin/setup_worker.cmd",pathRol+"/setup_worker.cmd");

			//añade las dependencias necesarias de cada rol
			zip.simpleUnzip(pathRol+"/node_modules","./bin/bin.zip").then(function(v){
				counter++;
				if(counter == numRoles())
					utils.remove("./tmp", true).then (function () { deferred.resolve(serviceDir); });
			});
			
		}

	} catch(err){
		util.error(err);
		deferred.reject(new Error(err));
	}

	return deferred.promise;
}


function buildServiceConfiguration( path , deploy, service ) {

	var deferred	= Q.defer();

	//procesamiento del ServiceConfiguration.Cloud.cscfg
	var serviceConfigurationXml = 	'<?xml version="1.0" encoding="utf-8"?>\n'+
									'<ServiceConfiguration '+
									'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '+
									'xmlns:xsd="http://www.w3.org/2001/XMLSchema" serviceName="'+service["name"]+'" '+
									'osFamily="1" osVersion="*" xmlns="http://schemas.microsoft.com/ServiceHosting/2008/10/ServiceConfiguration">\n';

	//Escribe la configuración de cada uno de los roles
	//indicando el número de instancias de cada uno de ellos, así como sus parametros de configuración
	//TODO: Dentro de ConfigurationSettings se han de introducir los parametros del JSON
	for( var rol in deploy.roles ){

		serviceConfigurationXml += '\t<Role name="'+rol+'">\n'+
		'\t\t<Instances count="'+deploy.roles[rol]+'" />\n'+
		'\t\t<ConfigurationSettings>\n';
      	// '\t\t\t<Setting name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" value="UseDevelopmentStorage=true" />\n'+
      	// <Setting name="<setting-name>" value="<setting-value>" />
    	//console.log("asd!");
		for (var par in service.roles[rol].parameterMap) {
			for (var par2 in deploy.parameters) {
				// console.log(deploy.parameters[par2]);
				if ( par2 == service.roles[rol].parameterMap[par] ) //console.log("Sugoi!! " + par2 + " == " + service.roles[rol].parameterMap[par]); 
					serviceConfigurationXml += '\t\t\t<Setting name="' + par + '" value="' + deploy.parameters[par2] + '"/>\n';
			}

		//	console.log(service.roles[rol].parameterMap[par]);
		//	console.log("par " + par);
		}
		//console.log("fgh");

      	
    	serviceConfigurationXml += '\t\t</ConfigurationSettings>\n'+
    	'\t\t<Certificates />\n'+
		'\t</Role>\n';

	}
	serviceConfigurationXml += '</ServiceConfiguration>';

	//escribe la cadena de configuración en el archivo xml
	try{

		fs.writeFileSync( path +"/ServiceConfiguration.Cloud.cscfg" , serviceConfigurationXml );
		fs.writeFileSync( path +"/ServiceConfiguration.Local.cscfg" , serviceConfigurationXml );

		deferred.resolve();
	
	}catch( err ){
		return deferred.reject( new Error( err ));
	}


	return deferred.promise;
}

function buildServiceDefinition( path , deploy , service )
{
	var deferred	= Q.defer();

	//procesamiento del ServiceDefinition.csdef
	var serviceDefinitionXml = '<?xml version="1.0" encoding="utf-8"?>\n';
		
	serviceDefinitionXml = 	'<ServiceDefinition xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '+
							'xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="'+ service["name"]+'" '+
							'xmlns="http://schemas.microsoft.com/ServiceHosting/2008/10/ServiceDefinition">\n';

	//define las propiedades de cada uno de los roles
	for(var rol in deploy.roles){
		//establece el tamaño de la maquina virtual a ejecutar con el rol
		serviceDefinitionXml += '\t<WorkerRole name="'+rol+'">\n';
		serviceDefinitionXml += startUpRol( );
		serviceDefinitionXml += params ( rol , service );
		serviceDefinitionXml += endpoints( rol , deploy , service );
		serviceDefinitionXml += runtime ( rol , service );
		serviceDefinitionXml +='\t</WorkerRole>\n';

	}
	serviceDefinitionXml += '</ServiceDefinition>';
	//se escribe la cadena de configuración en el archivo XML
	fs.writeFile( path +"/ServiceDefinition.csdef" , serviceDefinitionXml );
}

function startUpRol ( )
{
	var serviceDefinitionXml = '\t<Startup>\n';
    serviceDefinitionXml += '\t\t<Task commandLine="setup_worker.cmd" executionContext="elevated">\n';
    serviceDefinitionXml += '\t\t\t<Environment>\n';
    serviceDefinitionXml += '\t\t\t\t<Variable name="EMULATED">\n';
	serviceDefinitionXml += '\t\t\t\t\t<RoleInstanceValue xpath="/RoleEnvironment/Deployment/@emulated" />\n';
	serviceDefinitionXml += '\t\t\t\t</Variable>\n';
	serviceDefinitionXml += '\t\t\t</Environment>\n';
	serviceDefinitionXml += '\t\t</Task>\n';
	serviceDefinitionXml += '\t</Startup>\n';

	return serviceDefinitionXml;
}

function endpoints ( rol , deploy , service ){
	//escribe los endpoints vinculados al rol
	var serviceDefinitionXml = '\t<Endpoints>\n';

	//si el rol tiene definido algún endpoint de tipo público se escribe en la cadena del xml
	if ( service.roles[rol].endpointMap != undefined )
	{
		for( var namePort in service.roles[rol].endpointMap )
		{
			var port = deploy.endpoints[service.roles[rol].endpointMap[namePort]];
			serviceDefinitionXml += '\t\t<InputEndpoint name="'+namePort+'" protocol="tcp" port="'+port+'" />\n';
		}
			
	}

	//se introduce un endpoint interno utilizado para comunicación entre los diferentes procesos.
	//este endpoint no se encuentra definido dentro de los JSON.
	serviceDefinitionXml +=	'\t\t<InternalEndpoint name="internal" protocol="tcp" />\n'+
							'\t</Endpoints>\n';
	return serviceDefinitionXml;
}

function params ( rol, service ){

//      <Setting name="<setting-name>" />
	//escribe los endpoints vinculados al rol
	var serviceDefinitionXml = '\t<ConfigurationSettings>\n';

	//si el rol tiene definido algún endpoint de tipo público se escribe en la cadena del xml
	if ( service.roles[rol].parameterMap != undefined )
	{
		for( var par in service.roles[rol].parameterMap )
		{
			serviceDefinitionXml += '\t\t<Setting name="' + par + '" />\n';
		}
			
	}

	//se introduce un endpoint interno utilizado para comunicación entre los diferentes procesos.
	//este endpoint no se encuentra definido dentro de los JSON.
	serviceDefinitionXml +=	'\t</ConfigurationSettings>\n';
	
	return serviceDefinitionXml;
}

/*


		'\t\t<ConfigurationSettings>\n';
      	// '\t\t\t<Setting name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" value="UseDevelopmentStorage=true" />\n'+
      	// <Setting name="<setting-name>" value="<setting-value>" />
    	//console.log("asd!");
		for (var par in service.roles[rol].parameterMap) {
			for (var par2 in deploy.parameters) {
				// console.log(deploy.parameters[par2]);
				if ( par2 == service.roles[rol].parameterMap[par] ) //console.log("Sugoi!! " + par2 + " == " + service.roles[rol].parameterMap[par]); 
					serviceConfigurationXml += '\t\t\t<Setting name="' + par + '" value="' + deploy.parameters[par2] + '"/>\n';
			}

		//	console.log(service.roles[rol].parameterMap[par]);
		//	console.log("par " + par);
		}
		//console.log("fgh");

      	
    	serviceConfigurationXml += '\t\t</ConfigurationSettings>\n'+

*/

function runtime ( rol , service ){
	var xml =	'\t<Runtime>\n'+
    		  	'\t\t<Environment>\n';

   	if( service.roles[rol].endpointMap != undefined )
   	{
   		for( var namePort in service.roles[rol].endpointMap )
   		{
   			xml += 	'\t\t\t<Variable name="PORT">\n'+
          			'\t\t\t\t<RoleInstanceValue xpath="/RoleEnvironment/CurrentInstance/Endpoints/Endpoint[@name=\''+namePort+'\']/@port" />\n'+
        			'\t\t\t</Variable>\n';
   		}
   	}
   	// kkk revisar esto de "EMULATED"
    
    xml +=	'\t\t\t<Variable name="EMULATED">\n'+
         	'\t\t\t\t<RoleInstanceValue xpath="/RoleEnvironment/Deployment/@emulated" />\n'+
        	'\t\t\t</Variable>\n'+
     		'\t\t</Environment>\n'+
      		'\t\t<EntryPoint>\n'+
       		'\t\t\t<ProgramEntryPoint commandLine="node.exe .\\rol.js" setReadyOnProcessStart="true" />\n'+
      		'\t\t</EntryPoint>\n'+
   			'\t</Runtime>\n';

   	return xml;
}

/*
<ServiceConfiguration serviceName="<service-name>" osFamily=”[1|2]” osVersion="<os-version>">
  <Role name="<role-name>">
    <Instances count="<number-of-instances>" />    
    <ConfigurationSettings>
      <Setting name="<setting-name>" value="<setting-value>" />
    </ConfigurationSettings>
    <Certificates>
      <Certificate name="<certificate-name>" thumbprint="<certificate-thumbrint>" thumbprintAlgorithm="<algorithm>" />
    </Certificates> 
    <OsImage href="<vhd_image_name>" /> 
  </Role>
</ServiceConfiguration>
*/

exports.createStruct = createStruct;

