var fs = require ("fs");
var Q = require("q");
var util = require("util");
var exec = require('child_process').exec;
    

function copyFile ( org , dest ){
	
	var dataOrg = fs.readFileSync(org);
	fs.writeFileSync(dest, dataOrg);
}


// Parámetros.
// @file fichero o directorio a borrar.
// @recursive, si queremos borrado recursivo. Valores: true o false. 
function remove ( file , recursive ){

	util.debug(util.format("INIT {Remove:[file:%s , recursive:%s]}",file,recursive));

	var deferred = Q.defer();

	if(recursive == null || recursive == undefined || recursive == false){
		
		fs.unlink(file,function(err){
			console.log("Entra");
			if(err){
				deferred.reject(new Error( err ));
			}
			deferred.resolve(file);
			util.debug(util.format("END {Remove:[file:%s , recursive:%s]}",file,recursive));
		});
		

	}else{

		var cmd = "";

		if(process.platform == 'darwin' || process.platform == 'Darwin' || process.platform == 'Unix' || process.platform == 'unix')
			cmd = 'rm -r ' + file;
		else
			cmd = 'rd /s /q ' + file;

		var child = exec(cmd,function (error, stdout, stderr) {
			if( error ){
				deferred.reject( new Error( error ));
			}else{
				deferred.resolve(file);
			}

			util.debug(util.format("END {Remove:[file:%s , recursive:%s]}",file,recursive));
		});
	}


	return deferred.promise;
}

exports.copyFile = copyFile;
exports.remove = remove;