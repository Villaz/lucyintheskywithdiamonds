///Paquete para desempaquetar zips utilizando promesas.
///Queda pendiente adaptarlo a windows (utilizar \ en vez de /), para esto es necesario utilizar el paquete "os"
///utiliza la libreria "zip" / npm install zip

var zip = require("zip");
var fs = require("fs");
var os = require("os");
var Q = require("Q");
var util = require("util");
var pathLib = require("path");
var zipper = require('node-native-zip');


/**
* Comprime un directorio dentro de un fichero zip. El fichero zip tendrá el nombre de la carpeta comprimida
* @param {string}		[dir] 				Directorio a comprimir.
* @param {string}		[compressFile] 		Nombre del fichero comprimido.
*/
function zipFolder ( dir , compressFile ){

	if(compressFile == null || compressFile == undefined)
		compressFile = dir;

    var deferred = Q.defer();

    mapAllFiles( dir , function( path , stats , callback ){
        callback({ 
            name: path.replace( dir , "" ).substr( 1 ), 
            path: path 
        });
    },function( err , data ){

        var archive = new zipper();
        archive.addFiles(data,function(err){
            if( err )
                deferred.reject( new Error( err ) );
            else{
                var buff = archive.toBuffer();
                fs.writeFile( compressFile+'.zip' , buff , function( err ){
                    if( err )
                        deferred.reject( new Error( err ) );
                    else
                        deferred.resolve(compressFile+'.zip');
                });
            }
        });
    });

    return deferred.promise;
}

function nativeZip ( dir , compressFile ){

	util.debug(util.format("INIT:{Zip:[dir:%s , compressFile:%s]}",dir,compressFile));
	
	var deferred = Q.defer();

	if(compressFile == null || compressFile == undefined)
		compressFile = dir;

	var exec = require('child_process').exec;

	var command = util.format( 'zip -r %s ./%s/*' , compressFile , dir );

	util.debug(util.format("{Zip:[command:%s]}",command));

	child = exec(command,function (error, stdout, stderr) {
    	
    	if (error) {
    		util.debug(util.format("END ERROR:{Zip:[dir:%s , compressFile:%s]}",dir,compressFile));
    		deferred.reject(new Error(stdout));
    	}else{
    		util.debug(util.format("END:{Zip:[dir:%s , compressFile:%s]}",dir,compressFile));
    		deferred.resolve(dir);
    	}

    });

    return deferred.promise;
}


/*
* Descomprime una carpeta zip (un nivel).
*
*/
function simpleUnzip( dirName, file ){

	var deferred = Q.defer();
	
	util.debug(util.format("INIT:{SimpleUnzip:[dirName:%s , file:%s]}",dirName,file));
	fs.mkdirSync(dirName);
	
	//procede a la lectura del fichero zip
 	fs.readFile(file,function(err,data) {
 		// Si tenemos error, es que no podemos leer el fichero (Magia!)
 		if(err) { 
  			return deferred.reject( new Error (err + "El fichero no se puede leer."));
 		}

 		//lee los datos del zip
 		var output = zip.Reader(data);
 		var elements = filesInZip(data);
 		
 		var count = 1;

 		output.forEach(function(entry){ 
 			//nombre del fichero dentro del zip
 			var name = entry._header.file_name;
 			
 			name = dirName +"/"+ name;
 			name = name.replace("//","/");

 			//genera las carpetas y ficheros
 			var split = name.split("/");
 			if(split.length > 1){
 				var path = "";
 				for(var i = 0; i < split.length -1 ; i++)
 				{
 					try{
 						if(i == 0)
 							path = split[i];
 						else
 							path = path + "/" + split[i];

 						fs.mkdirSync(path);
 					}catch(e){
 						//si entra aqui el directorio ya existe. No es un fallo.
 					}
 				}
 			}
 			if(entry.isFile())
 				fs.writeFileSync(name,entry._stream);
 			count++;
 			if(count == elements){
 				util.debug(util.format("END:{SimpleUnzip:[dirName:%s , file:%s]}",dirName,file));
 				deferred.resolve(file);
			}
 		});
 	});
 	return deferred.promise;
}



/*
* Llamada iterativa para descomprimir una carpeta zip de varios niveles de compresión
*
*/
function unzipService(serviceFolder){

	
	var deferred = Q.defer();
	var tmp = "tmp";
	var counter = 0;


	
	simpleUnzip(tmp,serviceFolder).then(function( ){
		var files = fs.readdirSync(tmp);
		for(var file in files){
			if(files[file].slice(files[file].length-3,files[file].length) == 'zip'){
				var ori = tmp+"/"+files[file];
				var dest = ori.substring(0,ori.length-4);
				simpleUnzip(dest,ori).then(function(c){
					counter++;
					
					fs.unlinkSync(c);
					if(counter == files.length){
						deferred.resolve();
					}
				});
			}else{
				counter++;
				if(counter == files.length){
					deferred.resolve();
				}
			}
		}
	}).fail(function(e){
		console.log(e);
		deferred.reject(new Error(e));
	});
	return deferred.promise;
}

/*
* Desempaqueta un zip, creando la estructura de ficheros interna del mismo
* @param {dirName}, nombre que recibe la carpeta en la cual se descomprime el zip
* @param {file} , fichero zip a descomprimir.
* @param {path} , dirección relativa donde se encuentra el fichero, por defecto es undefined
* utilizado para descomprimir un zip interno a otro zip.
*
**/
function unzip (dirName,file,path){

	//genera la promesa
 	var deferred = Q.defer();

 	//promesas a utilizar para controlar
 	//las llamadas recursivas de unzip
 	//y la finalización correcta del nivel de unzip
 	//existe una promesa por cada zip incluido en el paquete.
 	var arrayDefered = new Array();
 	var arrayPromises = new Array();

 	//si path no se ha definido
 	//se define como una cadena vacia
 	if(path == undefined)
 	{
		path = "";
 	}
	


	//procede a la lectura del fichero zip
 	fs.readFile(file,function(err,data) {
 		// Si tenemos error, es que no podemos leer el fichero (Magia!)
 		if(err) { 
  			return deferred.reject( new Error (err + "El fichero no se puede leer."));
 		}

 		//lee los datos del zip
 		var output = zip.Reader(data);


 		//numero de elementos contenidos en el zip
 		//en caso de que el zip no sea correcto la promesa se rompe
 		try {
 			// número de ficheros, de cualquier tipo, dentro del zip principal
 			var elements = filesInZip(data);
 			elements = elements + 1; // Tenemos que tener en cuenta el directorio contenedor
 			// cuáles de ellos son .zip
 			var internalZips = filesZiped(data);
 		} catch(err) {
 			return deferred.reject(new Error(err + "Problemas en la lectura del zip."));
 		}

 		
 		

 		//inicializa los arrays con el número de promesas a satisfacer
 		//igual al número de elementos internos
 		//for ( var i = 0; i < internalZips; i++ ) {
 		for ( var i = 0; i < elements; i++ ) {	
 			arrayDefered[i] = Q.defer();
 			arrayPromises[i] = arrayDefered[i].promise;
 		}
 		
 		
 		//contador de número de elementos leidos
 		var count = 0;
 		//contador del número de elementos zip leidos
 		//var countZip = 0;

 		//recorre cada uno de los elementos contenidos en el zip
 		output.forEach(function(entry){ 
 			//nombre del fichero dentro del zip
 			var name = entry._header.file_name;
 			 
 			//si el nombre del fichero empieza por . o _ lo ignoramos, 
 			//ya que se trata de un fichero oculto del SO.
 			if(name.charAt(0) != '.' && name.charAt(0) != '_') {

 				var subName = dirName + name.substring(name.indexOf("/"));
 				var fileSplit = subName.split(".");

	 			

	 			//Si el fichero no contiene "." se trata de un directorio
 				if(fileSplit.length == 1)
				{
					//se crea la carpeta
					try {
						fs.mkdirSync ( path + dirName );
					} catch (err) {
						console.error("La carpeta " + path + dirName + " ya existe y pasamos de volverla a crear. " );
					}
					
					arrayDefered[count++].resolve();

				} 

				//si el fichero ha sido dividido en dos partes se trata de un fichero
				if(fileSplit.length > 1)
				{

					//se trata de un fichero zip
					if(fileSplit[1] == "zip")
					{
						//se escribe el fichero
						fs.writeFileSync(path + subName,entry._stream);
						

						var nameFolder = subName.substr(subName.lastIndexOf("/")+1).split(".")[0];

						var pathZip = subName.substr(0,subName.lastIndexOf("/")+1);

						
						//se descomprime el fichero zip y a continuación se elimina el mismo
						unzip(nameFolder,path+subName,path+pathZip).then(function(){						
							fs.unlinkSync(path + subName);
							arrayDefered[count++].resolve();
										
							//Si el número de elementos leidos es igual al máximo se da la 
							//función por finalizada
							
							if ( count == ( elements  ) ) {
								Q.all(arrayPromises).then(function()
								{
									deferred.resolve();
								},function(err)
								{
									defered.reject(new Error(err));
								});
				
							}

						},function(err){
							arrayDefered[count++].reject(new Error(err));
							deferred.reject(new Error(err));

							//Si el número de elementos leidos es igual al máximo se da la 
							//función por finalizada
							if ( count == ( elements  ) ) {
								Q.all(arrayPromises).then(function()
								{
									deferred.resolve();
								},function(err)
								{
									defered.reject(new Error(err));
								});
				
							}

						});

					} else if (fileSplit[1] == "json" || fileSplit[1] == "js") {
						//escribimos el fichero
						fs.writeFileSync(path+subName,entry._stream);
						//console.log(path+subName,entry._stream);
						arrayDefered[count++].resolve();
					}
					// El resto de ficheros los ignoramos
					else { 
						arrayDefered[count++].resolve();
					 }
				}
 			} else {
 				var subName = dirName + name.substring(name.indexOf("/"));
 				if ( subName.split("/").length < 3 ) {
 					arrayDefered[count++].resolve();
 				}
				//arrayDefered[count++].resolve();
			}
			

			//Si el número de elementos leidos es igual al máximo se da la 
			//función por finalizada
			if ( count == ( elements  ) ) {
				
				Q.all(arrayPromises).then(function()
				{
					deferred.resolve();
				},function(err)
					{
						defered.reject(new Error(err));
					});
			}

 		});
	

	});


 	//deferred.resolve();

 	return deferred.promise;
}


/**
* Retorna el número de elementos que contiene el fichero zip
* @param data , buffer recibido de la lectura del fichero
*/
/*
function filesInDir(data) {
	var contador = 0;
	var output = zip.Reader(data);	

	return function () {output.forEach( function () {
		contador++;
	});
	return contador;
	} 

} */


function filesInZip(data){
	var contador = 0;
	
	for(var a in zip.Reader(data).toObject())
 	{
 		contador++;
 	}
 	return contador;
}

/**
* Retorna el número de elementos zip que contiene el fichero zip
* @param data , buffer recibido de la lectura del fichero
*/
function filesZiped(data){
	var contador = 0;
	for(var a in zip.Reader(data).toObject())
 	{
 		if(a.split(".").length == 2 && a.split(".")[1] == 'zip')
 			contador++;
 	}
 	return contador;
}



//Código de cloud9

/**
 * Mapping function on all files in a folder and it's subfolders
 * @param dir {string} Source directory
 * @param action {Function} Mapping function in the form of (path, stats, callback), where callback is Function(result)
 * @param callback {Function} Callback fired after all files have been processed with (err, aggregatedResults)
 */
function mapAllFiles(dir, action, callback) {
    var output = [];
    
    fs.readdir(dir, function (err, files) {
        if (err) return callback(err);
        
        files = files && files.filter(function (f) { return !f.match(/^(\.git)/); });
            
        if (!files || !files.length) {
            return callback(null, output);
        }
                    
            
        var fileIx = 0;
        var fileErr = null, dirErr = null;
        
        function onFolderComplete(err, data) {
            if (err) { 
                dirErr = err;
            }
            
            fileIx += 1;
            
            if (data) {
                data.forEach(function (d) { output.push(d); });
            }
            
            if (fileIx === files.length) {
                return callback(dirErr, output);
            }
        }
        
        function onFileComplete(err) {
            if (err) { 
                fileErr = err;
            }
            
            fileIx += 1;
                        
            if (fileIx === files.length) {
                return callback(fileErr, output);
            }
        }

        files.forEach(function (file) {
            fs.stat(pathLib.join(dir, file), function (err, stats) {
                if (err) return onFileComplete(err);
                
                if (stats.isFile()) {
                    action(pathLib.join(dir, file), stats, function (res) {
                        if (res) {
                            output.push(res);
                        }
                        onFileComplete(null);
                    });
                }
                else if (stats.isDirectory()) {
                    mapAllFiles(pathLib.join(dir, file), action, onFolderComplete, output);
                }
            });
        });
        
    });    
}

exports.zip = nativeZip;
exports.unzip = unzip;
exports.simpleUnzip = simpleUnzip;
exports.unzipService = unzipService;