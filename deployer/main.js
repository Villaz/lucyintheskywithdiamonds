/*
Llamada al programa:

	node main.js deploy.json servicio.zip azure

	La ruta de los certificados y el fichero .publishSettings (sólo en el caso de azure) 
	será indicada en una variable de entorno CERT_DEPLOYMENT, 

*/

/**
* Pasos pendientes:
* Tratar una respuesta 404 en el caso de que falle la creación de CSPKG
* Es necesario conocer el identificador de la cuenta del usuario para conectrase a azure. ¿un nuevo Fichero JSON con los datos?¿Otra variable de entorno?.
* Es necesario que el usuario introduzca la contraseña con la cual se encuentra protegido el certificado pfx . ¿Comandos? ¿fichero JSON?. ¿Variable de entorno?
* Modificar la primera linea de uploadAzure para que recoga los certificados correspondientes.
*
*/

var fs = require ("fs");
var path = require ("path");
var zip = require ("./lib/zip.js");
var Q = require ("Q");
var https = require("https");
var util = require('util');
var xml2js = require('xml2js');
var struct = require("./lib/struct.js");
var utils = require('./lib/utils.js');
var Rest = require('./lib/rest.js');

var user_id = "";
var service_name = "";
var dir_certs = "";

// 1 Comprobación parámetro

// if (process.argv.length != 4){ Son cinco los argv, no 4
if (process.argv.length != 5){ 
	console.error("Número de parámetros incorrecto");
	console.error("Uso: node main.js deploy.js servicio.zip azure");
	process.exit(1);
}else{
	var deploy = process.argv[2];
	var service = process.argv[3];

	if(!path.existsSync(deploy)){
		console.error("No se encuentra el fichero con la configuración de despliegue: " + deploy);
		process.exit(2);
	}

	if(!path.existsSync(service)){
		console.error("No se encuentra el fichero del servicio comprimido: "+ service);
		process.exit(2);
	}

	// Comprobar si existe la variable de entorno con la ruta de los certificados, mirar alguna forma de validarlos

	var generateStruct = function() { return struct.createStruct(deploy);}
	var zipStruct = function(dir) { return zip.zip(dir,"upload");}
	var removeFolder = function(dir){ return utils.remove(dir,true);}

	//zip.unzip("tmp", service)
	//	.then(generateStruct)
//		.then(zipStruct)
	//	.then(removeFolder)
	//	.then(requestCspkgService)
		getCerts()
		.then(getAzureUserID)
		.then(uploadAzure)
		.then(function(v){console.log("Todo perfecto")})
		.fail(function(e){console.log(e)});

}


function getCerts() {
	var deferred = Q.defer();
	var exec = require('child_process').exec;
	var child;

	
	child = exec('echo $CERT_DEPLOYMENT',
  		function (error, stdout, stderr) {
    	
    	if (stdout == null) {
    		error = "La ruta de los certificados no ha sido indicada correctamente. Comprobar variable de entorno CERT_DEPLOYMENT.";
    		deferred.reject(error);
    	}
    	dir_certs = stdout.replace('\n','');
    	deferred.resolve(dir_certs);

    });
	
	return deferred.promise;
}

function copyCerts(serviceDir){
	var deferred = Q.defer();

	getCerts().then(function(dir){
		var files = fs.readdirSync(dir);
		for(var file in files)
		{
			utils.copyFile(dir+"/"+files[file],serviceDir+"/"+files[file]);
		}
		deferred.resolve(serviceDir);
		
	});

	return deferred.promise;
}


/**
* Retorna el valor del user id de Azure.
*/
function getAzureUserID() {
	var deferred = Q.defer();
	var exec = require('child_process').exec;
	var child;

	
	
	child = exec('echo $AZURE_USER_ID',
  		function (error, stdout, stderr) {
    	
    	if (stdout == null) {
    		console.error("No se encuentra la variable de entorno AZURE_USER_ID");
    		error = "No se encuentra la variable de entorno AZURE_USER_ID";
    		deferred.reject(error);
    	}
    	user_id = stdout.replace('\n','');
    	deferred.resolve(user_id);

    });
	
	return deferred.promise;
}



/**
* Realiza la petición request para generar el paquete cspkg.
*/
function requestCspkgService( serviceName ){
	
	util.debug(util.format("INIT {requestCspkgService:[serviceName:%s]} ",serviceName) );

	service_name = serviceName;

	var deferred = Q.defer();

	//lee el fichero zip y se codifica en base64 para que se pueda enviar dentro de un XML.
	var base64Data = fs.readFileSync('upload.zip').toString('base64');

	//generación del cuerpo del XML para la petición REST
	var data = '<?xml version="1.0" encoding="utf-8"?>';
	data += '<AzureTest>';
	data += '<serviceName>'+serviceName+'</serviceName>';
	data += '<zipCspkg>'+base64Data+'</zipCspkg>'
	data += '</AzureTest>';


	//opciones para realizar la peticion
	//Host donde se encuentra el servicio
	//puerto del servicio SSL
	//ruta del servicio a consumir.
	//metodo del servicio. POST
	var options = {
  		host: 'packGenerator.cloudapp.net',
  		port: 443,
  		path: '/cspkg',
  		method: 'POST'
	};


	var req = https.request( options , function( res ){
		var data = "";
		//se debe comprobar si el estado de la respuesta es 404, en cuyo caso se trataria de manera diferente
		res.on( 'data' , function( d ){
			data += d;
		});

		res.on ( 'end' , function ( ){
			var parser = new xml2js.Parser();
			
			parser.parseString( data , function ( err , result ) {
				if(err){
					util.debug("END ERROR {requestCspkgService} ");
					deferred.reject( new Error( err ));
				} else{
					var resultCspkg = new Buffer(result["cspkg"], 'base64');
					var config = new Buffer(result["configuration"], 'base64');
				
					fs.writeFileSync("upload.cspkg",resultCspkg);	
					fs.writeFileSync("ServiceConfiguration.cscfg",config);

					util.debug("END {requestCspkgService} ");

					deferred.resolve("upload.cspkg");
				}
			
			});
		});
	});
	
	req.setHeader( "Content-Type" , "application/xml" );
	req.write( data );
	req.end();

	req.on( 'error' , function( e ){
		util.debug("END ERROR {requestCspkgService} ");
		deferred.reject( new Error( e ));
	});

	return deferred.promise;
}


function uploadAzure( ){

	var deferred = Q.defer();

	//eliminar esta linea cuando se pase a produccion
	service_name = "letiservice";

	util.debug(util.format("INIT {uploadAzure:[user_id:%s,service_name:%s",user_id,service_name));

	var azu = new Rest(user_id,dir_certs+'/cert.pem',dir_certs+'/key.pem');
	
	
	var createStorage = function () { return azu.createStorage( service_name+"storage")};
	var createCertificate = function() { return azu.addCertificate(dir_certs+"/cert.pfx","holahola",service_name);}
	var createBlobContainer = function() {return azu.createBlobContainer(service_name,service_name+"storage");}
	var uploadFile = function(){ return azu.uploadFile(service_name+"storage",service_name,'package','upload.cspkg');}
	var createDeployment = function(){ return azu.createDeployment(service_name,util.format("http://%s.blob.core.windows.net/%s/package",service_name+"storage",service_name),service_name,"ServiceConfiguration.cscfg");}

	azu.createService(service_name)
	.then(createCertificate)
	.then(createStorage)
	.then(createBlobContainer)
	.then(uploadFile)
	.then(createDeployment)
	.then(function(v){ deferred.resolve(v);})
	.fail(function(err){deferred.reject(err)});

	return deferred.promise;

}

