var azure = require("azure");
var net   = require("net");
var http  = require("http");
//var q     = require("q");

var path = require('path');
var uuid = require('node-uuid');
var util = require('util');
var nconf = require('nconf');
var _ = require('underscore');


var RoleEnvironment = azure.RoleEnvironment;

// Table service 'constants'
var TABLE_NAME = 'roles';
var DEFAULT_PARTITION = 'roles';

// Blob service 'constants'
var CONTAINER_NAME = 'roles';

nconf.file({ file: 'config.json' });
var tableClient = azure.createTableService();

exports = module.exports;

exports.hola = function(){
	console.log("asd");
}

console.log("fgh");
  var port = process.env.port || 1337;
  http.createServer(function (req, res) {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Hello World\n');
}).listen(port);


// falta incluir el m�todo que nos devuelva la informaci�n de los par�metros
// despu�s, pasarlo todo a Q

exports.currentRole = function(callback){
	azure.RoleEnvironment.getCurrentRoleInstance(function(error,instance){
		if(error){
			callback(error);
		}else{
			callback(undefined,instance);
		}
	});
}

exports.roles = function (callback) {
  azure.RoleEnvironment.getRoles(function (error, roles) {
    if (error) {
      callback(error);
    } else {
		callback(undefined,roles);
    }
  });
};

exports.instancesFromRole = function(name,callback){
	exports.roles(function(err,roles){
		if(err)
		{
			callback(err);
		}else
		{
			var found = false;
			for(var rol in roles)
			{
				if(rol == name)
				{
					found = true;
					callback(undefined,roles[rol].instances);
				}
			}
			if(!found)
			{
				callback("unknown role");
			}
		}
	});
}

exports.EndpointFromRole = function (nameRole,endpointName,callback)
{
	exports.instancesFromRole(nameRole,function(err,instances)
	{
		if(err)
		{
			callback(err);
		}
		else{
			var instanceEndpoint = {}
			for(var instance in instances)
			{
				if(instances[instance].endpoints[endpointName])
				{
					instanceEndpoint[instance] = {"port":instances[instance].endpoints[endpointName].port, "address":instances[instance].endpoints[endpointName].address};
				}
			}
			callback(undefined,instanceEndpoint);
		}
	});
}


//mirar fault domains
exports.getListenAddress = function(endpointName, callback) {
	RoleEnvironment.getCurrentRoleInstance(function( inErr, instance) {
		if (inErr || !instance['endpoints'] || ! instance['endpoints'][endpointName]) {
			callback("Error, could not get current role instance endpoint '"+ endpointName + "', error: " + inErr + "\r\n", null, null);
		} else {
            var currentEndpoint = instance['endpoints'][endpointName];
            callback(null, currentEndpoint['address'], currentEndpoint['port']);
        }
    }); 
}


exports._getRoleData = function (roleName, callback) {
  var tableQuery = azure.TableQuery
    .select()
    .from(TABLE_NAME)
    .where('name eq ?', roleName);

  tableClient.queryEntities(tableQuery, function (error, entities) {
    callback(error, _.first(entities));
  });
};

exports._getRolesData = function (callback) {
  var tableQuery = azure.TableQuery
    .select()
    .from(TABLE_NAME);

  tableClient.queryEntities(tableQuery, callback);
};

