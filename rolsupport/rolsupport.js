var azure = require("azure");
var net   = require("net");
var http  = require("http");
var Q     = require("q");



/** COSAS A CONSIDERAR **/
	// kkk el nombre del rol no deber�a ser hardcoded. A pensar...
	// kkk por defecto consideraremos un rol interno, el cual llamaremos desde rolsupport con una llamada particular (transparencia)



/**
* Retorna informaci�n sobre el rol actual
*/
function currentRole(){
	var deferred = Q.defer();
	azure.RoleEnvironment.getCurrentRoleInstance(function(error,instance){
		if(error){
			deferred.reject(new Error(error ) ):
		}else{
			deferred.resolve(instance);
	
		}
	});

	return deferred.promise;
}

/**
* Retorna una lista de todos los roles definidos en el servicio
*/
function roles( ){
	var deferred = Q.defer();
	azure.RoleEnvironment.getRoles(function (error, roles) {
    	if (error) {
      		deferred.reject( new Error( error ) );
    	} else {
			deferred.resolve( roles );
    	}
  	});
	return deferred.promise;
}

/*
/* Devuelve todas las instancias de un rol, 
/* a partir del nombre del rol y del listado de todas las instancias de todos los roles
/* @param name [String]	Nombre del rol
*/
function instancesFromRole( name ){
	var deferred = Q.defer();

	exports.roles(function(err,roles){
		if(err)
		{
			deferred.reject( new Error( error ) );
		}else
		{
			var found = false;
			for(var rol in roles)
			{
				if(rol == name)
				{
					found = true;
					deferred.resolve( roles[rol].instances );
				}
			}
			if(!found)
			{
				deferred.reject( new Error( "unknown role" ) );
			}
		}
	});
	return deferred.promise;
}


/**Le das el nombre del rol, el nombre del endpoint y te devuelve todas 
/*las direcciones y puertos de ese endpoint en todas las instancias
/*@param nameRole		[String]	Nombre del rol del cual se desea conocer el endpoint
/*@param endpointName	[String] 	Nombre del endpoint del que se quiere conocer los datos direcci�n-puerto
*/
function EndpointFromRole ( nameRole , endpointName )
{
	var deferred = Q.defer();


	var instances = function(instances){
		var instanceEndpoint = {}
		for(var instance in instances)
		{
			if(instances[instance].endpoints[endpointName])
			{
				instanceEndpoint[instance] = {"port":instances[instance].endpoints[endpointName].port, "address":instances[instance].endpoints[endpointName].address};
			}
		}

		deferred.resolve(instanceEndpoint);
	}

	var failure = function(error){
		deferred.reject(error);
	}

	instancesFromRole( nameRole ).then(intances).fail(failure);

	return deferred.promise;
		
}

/**
/* Retorna la direcci�n y el puerto donde se encuentra el endpoin dado.
/* @param endpointName 	[String] nombre del endpoint
/* @return 				[promise] contiene un array con los identificadores 'address' y 'port'
**/
function getListenAddress ( endpointName )
{
	var deferred = Q.defer();

	RoleEnvironment.getCurrentRoleInstance(function( inErr, instance) {
		if (inErr || !instance['endpoints'] || ! instance['endpoints'][endpointName]) {
			deferred.reject(new Error( "Error, could not get current role instance endpoint '"+ endpointName + "', error: " + inErr ));
		} else {
            var currentEndpoint = instance['endpoints'][endpointName];

            //currentEndpoint['address']
            //currentEndpoint['port']
            deferred.resolve(currentEndpoint);
        }
    }); 


	return deferred.promise;
}

function getSettings ( ){

	var deferred = Q.defer();

	RoleEnvironment.getConfigurationSettings = function (callback) {
  		validateNodeVersion();
 	 	validateCallback(callback);

  		RoleEnvironment._initialize(function (error) {
  			if (error) {
    			deferred.reject( new Error( error ));
    		} else {
    			deferred.resolve(currentEnvironmentData.configurationSettings );
    		}
  		});
	};

	return deferred.promise;
}
